function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
    <div class="col">
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-text"><small class="text-muted">${location}</small></p>
          <p class="card-text">${description}</p>
        </div>
        </ul>
        <div class="card-footer">
            ${startDate.toLocaleDateString()} - ${endDate.toLocaleDateString()}
        </div>
      </div>
    </div>
    `;
  }


  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            console.log(details)
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date(details.conference.starts)
            const endDate = new Date(details.conference.ends)
            const location = details.conference.location.name;
            const html = createCard(title, description, pictureUrl, startDate, endDate, location);
            const row = document.querySelector('.row');
            row.innerHTML += html;
          }
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
    }

  });
